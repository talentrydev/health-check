# Health check library and Symfony bundle

This module provides essential tools for exposing a health check endpoint in your app. It can be used in vanilla php code,
but for maximum ease of use, it's recommended to make use of the provided symfony bundle.

## Installing and configuring the symfony bundle

* Run:

```
composer require talentrydev/health-check
```

* Add the symfony bundle to your kernel's `registerBundles` method:

```
return [
    //...
    new \Talentry\HealthCheck\SymfonyBundle\HealthCheckBundle();
]; 
```

* Configure the bundle by adding the following to your symfony config.yaml file. See below for all available options
and their default values (everything is optional).

```
health_check:
  service_name: API //this is the name of the root service that will appear in the health check endpoint response
  mysql:
    enabled: true //set to false to disable the mysql health checker
    host: localhost
    port: 3306
    user: root
    password: root
    database: mysql
  redis:
    enabled: true //set to false to disable the redis health checker
    host: localhost
    port: 6379
```

* Add the following you your routing config:

```
healthcheck:
    resource: "@HealthCheckBundle/Resources/config/routing.yml"
```

## Using the health check endpoint

Make a GET HTTP request to <base_url>/healthcheck

This will perform a health check of the application itself, but not of the dependent services.
To also perform a health check on these services, add them to the URL like so:

```
<base_url>/healthcheck?mysql=1&redis=1
```

or specify `dependencies=1` to run a health check on all dependencies:

```
<base_url>/healthcheck?dependencies=1
```

## Extending the bundle

To create your own health checker, simply implement `Talentry\HealthCheck\HealthChecker\HealthChecker` interface.
The name you specify in the return of the `getServiceName` is the name you will pass to the health check endpoint.
