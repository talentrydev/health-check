<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\Tests\Controller;

use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Talentry\HealthCheck\HealthChecker\MysqlHealthChecker;

class HealthCheckControllerTest extends WebTestCase
{
    private KernelBrowser $kernelBrowser;

    protected function setUp(): void
    {
        parent::setUp();

        $this->kernelBrowser = $this->createClient();
    }

    public function testHealthActionWithNoArguments(): void
    {
        $this->kernelBrowser->request('GET', '/healthcheck');
        $response = $this->kernelBrowser->getResponse();
        self::assertSame(200, $response->getStatusCode());
        self::assertSame(json_encode(['status' => '👌']), $response->getContent());
    }

    public function testHealthActionWithArguments(): void
    {
        $this->kernelBrowser->request('GET', '/healthcheck?mysql=1&redis=1');
        $response = $this->kernelBrowser->getResponse();
        self::assertSame(200, $response->getStatusCode());

        $expectedResponse = [
            'service' => 'API',
            'healthy' => true,
            'dependencies' => [
                [
                    'service' => 'mysql',
                    'healthy' => true,
                    'details' => [],
                ],
                [
                    'service' => 'redis',
                    'healthy' => true,
                    'details' => [],
                ],
            ],
        ];

        self::assertSame(json_encode($expectedResponse), $response->getContent());
    }

    public function testHealthActionWithUnhealthyService(): void
    {
        $mysqlHealthChecker = self::getContainer()->get(MysqlHealthChecker::class);
        $rc = new ReflectionClass($mysqlHealthChecker);
        $host = $rc->getProperty('host');
        $host->setAccessible(true);
        $host->setValue($mysqlHealthChecker, 'invalid');
        $this->kernelBrowser->request('GET', '/healthcheck?mysql=1&redis=1');
        $response = $this->kernelBrowser->getResponse();

        self::assertSame(500, $response->getStatusCode());
    }

    public function testHealthActionWithAllDependencies(): void
    {
        $this->kernelBrowser->request('GET', '/healthcheck?dependencies=1');
        $response = $this->kernelBrowser->getResponse();
        self::assertSame(200, $response->getStatusCode());

        $expectedResponse = [
            'service' => 'API',
            'healthy' => true,
            'dependencies' => [
                [
                    'service' => 'mysql',
                    'healthy' => true,
                    'details' => [],
                ],
                [
                    'service' => 'redis',
                    'healthy' => true,
                    'details' => [],
                ],
            ],
        ];

        self::assertSame(json_encode($expectedResponse), $response->getContent());
    }
}
