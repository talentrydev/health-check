<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\Tests\Factory;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Talentry\HealthCheck\Factory\HealthCheckerFactory;
use Talentry\HealthCheck\Factory\UnsupportedServiceException;
use Talentry\HealthCheck\HealthChecker\CompositeHealthChecker;

class HealthCheckerFactoryTest extends KernelTestCase
{
    private HealthCheckerFactory $factory;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        $this->factory = self::getContainer()->get(HealthCheckerFactory::class);
    }

    public function testGenerateWithNoArguments(): void
    {
        /** @var CompositeHealthChecker $healthCheck */
        $healthCheck = $this->factory->generate();

        $healtcheckers = ['mysql', 'redis'];
        self::assertInstanceOf(CompositeHealthChecker::class, $healthCheck);
        foreach ($healtcheckers as $healtchecker) {
            self::assertContains($healtchecker, $healthCheck->getDependentServiceNames());
        }
    }

    public function testGenerateWithArguments(): void
    {
        /** @var CompositeHealthChecker $healthCheck */
        $healthCheck = $this->factory->generate(['mysql', 'redis']);

        self::assertInstanceOf(CompositeHealthChecker::class, $healthCheck);
        self::assertEquals(['mysql', 'redis'], $healthCheck->getDependentServiceNames());
    }

    public function testGenerateWithInvalidServiceName(): void
    {
        self::expectException(UnsupportedServiceException::class);

        $this->factory->generate(['unknown-service']);
    }
}
