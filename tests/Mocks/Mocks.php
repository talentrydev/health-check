<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\Tests\Mocks;

use Talentry\HealthCheck\HealthChecker\HealthChecker;
use Talentry\HealthCheck\HealthReport\HealthReport;
use Talentry\HealthCheck\HealthReport\HealthReportBuilder;

class Mocks
{
    public static function createHealthChecker(string $serviceName, bool $healthy): HealthChecker
    {
        return new class ($serviceName, $healthy) implements HealthChecker {
            public function __construct(private readonly string $serviceName, private readonly bool $healthy)
            {
            }

            public function getServiceName(): string
            {
                return $this->serviceName;
            }

            public function getHealthReport(): HealthReport
            {
                $builder = new HealthReportBuilder();
                $builder->setServiceName($this->serviceName);
                if ($this->healthy) {
                    $builder->setHealthy();
                } else {
                    $builder->setUnhealthy();
                }

                return $builder->buildHealthReport();
            }
        };
    }
}
