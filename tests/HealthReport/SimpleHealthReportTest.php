<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\Tests\HealthReport;

use PHPUnit\Framework\TestCase;
use Talentry\HealthCheck\HealthReport\SimpleHealthReport;

class SimpleHealthReportTest extends TestCase
{
    public function testToArray(): void
    {
        $report = new SimpleHealthReport('test', true);

        self::assertSame(['service' => 'test', 'healthy' => true, 'details' => []], $report->toArray());

        $report = new SimpleHealthReport('test', false);

        self::assertSame(['service' => 'test', 'healthy' => false, 'details' => []], $report->toArray());

        $report = new SimpleHealthReport('test', false, ['implementation' => 'imp']);

        $expectedArray = [
            'service' => 'test',
            'healthy' => false,
            'details' => [
                'implementation' => 'imp',
            ],
        ];

        self::assertSame($expectedArray, $report->toArray());
    }
}
