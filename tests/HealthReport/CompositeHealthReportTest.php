<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\Tests\HealthReport;

use PHPUnit\Framework\TestCase;
use Talentry\HealthCheck\HealthReport\CompositeHealthReport;
use Talentry\HealthCheck\HealthReport\HealthReport;
use Talentry\HealthCheck\HealthReport\SimpleHealthReport;

class CompositeHealthReportTest extends TestCase
{
    private HealthReport $firstHealthyServiceReport;
    private HealthReport $secondHealthyServiceReport;
    private HealthReport $unhealthyServiceReport;

    protected function setUp(): void
    {
        parent::setUp();

        $this->firstHealthyServiceReport = new SimpleHealthReport('healthy 1', true);
        $this->secondHealthyServiceReport = new SimpleHealthReport('healthy 2', true);
        $this->unhealthyServiceReport = new SimpleHealthReport('unhealthy', false);
    }

    public function testIsHealthyWhenAllComponentsAreHealthy(): void
    {
        $composite = new CompositeHealthReport('');
        $composite->addComponent($this->firstHealthyServiceReport);
        $composite->addComponent($this->secondHealthyServiceReport);

        self::assertTrue($composite->isHealthy());
    }

    public function testIsHealthyWhenNotAllComponentsAreHealthy(): void
    {
        $composite = new CompositeHealthReport('');
        $composite->addComponent($this->firstHealthyServiceReport);
        $composite->addComponent($this->unhealthyServiceReport);

        self::assertFalse($composite->isHealthy());
    }

    public function testGetServiceName(): void
    {
        $serviceName = 'service';
        $composite = new CompositeHealthReport($serviceName);

        self::assertSame($serviceName, $composite->getServiceName());
    }

    public function testGetDependencies(): void
    {
        $composite = new CompositeHealthReport('');
        $composite->addComponent($this->firstHealthyServiceReport);
        $composite->addComponent($this->secondHealthyServiceReport);

        $dependencies = $composite->getDependencies();
        self::assertCount(2, $dependencies);
        self::assertInstanceOf(HealthReport::class, $dependencies[0]);
        self::assertInstanceOf(HealthReport::class, $dependencies[1]);

        self::assertSame($this->firstHealthyServiceReport->getServiceName(), $dependencies[0]->getServiceName());
        self::assertSame($this->secondHealthyServiceReport->getServiceName(), $dependencies[1]->getServiceName());
    }

    public function testToArray(): void
    {
        $composite = new CompositeHealthReport('talentry');
        $composite->addComponent($this->firstHealthyServiceReport);
        $composite->addComponent($this->secondHealthyServiceReport);

        $array = $composite->toArray();
        $expectedArray = [
            'service' => 'talentry',
            'healthy' => true,
            'dependencies' => [
                [
                    'service' => $this->firstHealthyServiceReport->getServiceName(),
                    'healthy' => $this->firstHealthyServiceReport->isHealthy(),
                    'details' => [],
                ],
                [
                    'service' => $this->secondHealthyServiceReport->getServiceName(),
                    'healthy' => $this->secondHealthyServiceReport->isHealthy(),
                    'details' => [],
                ],
            ],
        ];

        self::assertSame($expectedArray, $array);
    }
}
