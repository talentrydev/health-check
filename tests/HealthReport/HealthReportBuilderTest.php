<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\Tests\HealthReport;

use PHPUnit\Framework\TestCase;
use Talentry\HealthCheck\HealthReport\HealthReportBuilder;

class HealthReportBuilderTest extends TestCase
{
    private HealthReportBuilder $builder;

    protected function setUp(): void
    {
        parent::setUp();

        $this->builder = new HealthReportBuilder();
    }

    public function testBuildSimpleHealthReport(): void
    {
        $this->builder->setServiceName('test');
        $this->builder->setHealthy();

        $report = $this->builder->buildHealthReport();

        self::assertTrue($report->isHealthy());
        self::assertSame('test', $report->getServiceName());
    }
}
