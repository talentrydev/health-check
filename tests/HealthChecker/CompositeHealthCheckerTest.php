<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\Tests\HealthChecker;

use PHPUnit\Framework\TestCase;
use Talentry\HealthCheck\HealthChecker\CompositeHealthChecker;
use Talentry\HealthCheck\HealthReport\HealthReport;
use Talentry\HealthCheck\Tests\Mocks\Mocks;

class CompositeHealthCheckerTest extends TestCase
{
    private CompositeHealthChecker $compositeHealthChecker;
    private string $compositeServiceName;
    private string $firstDependencyName;
    private string $secondDependencyName;

    protected function setUp(): void
    {
        parent::setUp();

        $this->firstDependencyName = 'service 1';
        $this->secondDependencyName = 'service 2';
        $this->compositeServiceName = 'composite';

        $healthChecker1 = Mocks::createHealthChecker($this->firstDependencyName, true);
        $healthChecker2 = Mocks::createHealthChecker($this->secondDependencyName, true);

        $this->compositeHealthChecker = new CompositeHealthChecker($this->compositeServiceName);
        $this->compositeHealthChecker->addComponent($healthChecker1);
        $this->compositeHealthChecker->addComponent($healthChecker2);
    }

    public function testGetStatusReport(): void
    {
        $compositeReport = $this->compositeHealthChecker->getHealthReport();

        self::assertSame($this->compositeServiceName, $compositeReport->getServiceName());

        $dependencies = $compositeReport->getDependencies();
        self::assertCount(2, $dependencies);
        self::assertInstanceOf(HealthReport::class, $dependencies[0]);
        self::assertInstanceOf(HealthReport::class, $dependencies[1]);
        self::assertSame($this->firstDependencyName, $dependencies[0]->getServiceName());
        self::assertSame($this->secondDependencyName, $dependencies[1]->getServiceName());
    }
}
