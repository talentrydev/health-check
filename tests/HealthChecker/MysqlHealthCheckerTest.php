<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\Tests\HealthChecker;

use PHPUnit\Framework\Attributes\DataProvider;
use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Talentry\HealthCheck\HealthChecker\MysqlHealthChecker;

class MysqlHealthCheckerTest extends KernelTestCase
{
    private MysqlHealthChecker $healthChecker;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        $this->healthChecker = self::getContainer()->get(MysqlHealthChecker::class);
    }

    public function testWithHealthyService(): void
    {
        $report = $this->healthChecker->getHealthReport();

        self::assertSame('mysql', $report->getServiceName());
        self::assertTrue($report->isHealthy());
    }

    #[DataProvider('dataProviderForTestWithInvalidConnectionParameters')]
    public function testWithInvalidConnectionParameters(
        ?string $host = null,
        ?int $port = null,
        ?string $username = null,
        ?string $password = null,
        ?string $database = null
    ): void {
        if ($host) {
            $this->setHealthCheckerProperty('host', $host);
        }

        if ($port) {
            $this->setHealthCheckerProperty('port', $port);
        }

        if ($username) {
            $this->setHealthCheckerProperty('username', $username);
        }

        if ($password) {
            $this->setHealthCheckerProperty('password', $password);
        }

        if ($database) {
            $this->setHealthCheckerProperty('database', $database);
        }

        $report = $this->healthChecker->getHealthReport();

        self::assertSame('mysql', $report->getServiceName());
        self::assertFalse($report->isHealthy());
    }

    public static function dataProviderForTestWithInvalidConnectionParameters(): array
    {
        return [
            ['unknown-host',    null,   null,               null,           null                ],
            [null,              80,     null,               null,           null                ],
            [null,              null,   'unknown-user',     null,           null                ],
            [null,              null,   null,               'bad-password', null                ],
            [null,              null,   null,               null,           'unknown-database'  ],
        ];
    }

    private function setHealthCheckerProperty(string $name, $value): void
    {
        $rc = new ReflectionClass($this->healthChecker);
        $prop = $rc->getProperty($name);
        $prop->setAccessible(true);
        $prop->setValue($this->healthChecker, $value);
    }
}
