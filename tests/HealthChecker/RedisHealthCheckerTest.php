<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\Tests\HealthChecker;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Talentry\HealthCheck\HealthChecker\RedisHealthChecker;

class RedisHealthCheckerTest extends KernelTestCase
{
    private RedisHealthChecker $healthChecker;

    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->healthChecker = self::getContainer()->get(RedisHealthChecker::class);
    }

    public function testGetStatusWithHealthyService(): void
    {
        $statusReport = $this->healthChecker->getHealthReport();

        self::assertTrue($statusReport->isHealthy());
        self::assertSame('redis', $statusReport->getServiceName());
    }

    public function testGetStatusWithUnhealthyService(): void
    {
        $healthCheck = new RedisHealthChecker('invalid-host');
        $statusReport = $healthCheck->getHealthReport();

        self::assertFalse($statusReport->isHealthy());
        self::assertSame('redis', $statusReport->getServiceName());
    }
}
