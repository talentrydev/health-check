<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\HealthChecker;

use Talentry\HealthCheck\HealthReport\CompositeHealthReport;
use Talentry\HealthCheck\HealthReport\HealthReport;

class CompositeHealthChecker implements HealthChecker
{
    /**
     * @var HealthChecker[]
     */
    private array $components = [];

    public function __construct(
        private readonly string $serviceName,
    ) {
    }

    public function addComponent(HealthChecker $component): void
    {
        $this->components[] = $component;
    }

    public function getServiceName(): string
    {
        return $this->serviceName;
    }

    /**
     * @return string[]
     */
    public function getDependentServiceNames(): array
    {
        $serviceNames = [];
        foreach ($this->components as $component) {
            $serviceNames[] = $component->getServiceName();
        }

        return $serviceNames;
    }

    public function getHealthReport(): HealthReport
    {
        $report = new CompositeHealthReport($this->serviceName);

        foreach ($this->components as $component) {
            $report->addComponent($component->getHealthReport());
        }

        return $report;
    }
}
