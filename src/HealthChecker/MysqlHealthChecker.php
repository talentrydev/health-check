<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\HealthChecker;

use Exception;
use PDO;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Talentry\HealthCheck\HealthReport\HealthReport;
use Talentry\HealthCheck\HealthReport\HealthReportBuilder;

class MysqlHealthChecker implements HealthChecker
{
    /**
     * Most of the arguments cannot be made readonly, because they are modified in tests
     */
    public function __construct(
        private string $host,
        private int $port,
        private string $username,
        private string $password,
        private string $database,
        private readonly int $timeout = 1,
        private readonly LoggerInterface $logger = new NullLogger(),
    ) {
    }

    public function getServiceName(): string
    {
        return 'mysql';
    }

    public function getHealthReport(): HealthReport
    {
        $builder = new HealthReportBuilder();
        $builder->setServiceName($this->getServiceName());
        try {
            $pdo = $this->createPdo();
            $stmt = $pdo->prepare('SELECT DATABASE()');
            if ($stmt->execute() && $stmt->fetchColumn(0) === $this->database) {
                $builder->setHealthy();
            }
        } catch (Exception $e) {
            $builder->setUnhealthy();
            $this->logger->error($e->getMessage(), ['exception' => $e]);
        }

        return $builder->buildHealthReport();
    }

    private function createPdo(): PDO
    {
        $dsn = sprintf('mysql:dbname=%s;host=%s;port=%s', $this->database, $this->host, $this->port);
        $options = [
            PDO::ATTR_TIMEOUT => $this->timeout,
        ];

        return new PDO($dsn, $this->username, $this->password, $options);
    }
}
