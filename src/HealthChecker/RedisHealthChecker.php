<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\HealthChecker;

use Exception;
use Predis\Client;
use Predis\ClientInterface;
use Predis\Response\Status;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Talentry\HealthCheck\HealthReport\HealthReport;
use Talentry\HealthCheck\HealthReport\HealthReportBuilder;

class RedisHealthChecker implements HealthChecker
{
    private ClientInterface $client;

    public function __construct(
        string $host,
        int $port = 6379,
        int $timeout = 1,
        private readonly LoggerInterface $logger = new NullLogger(),
    ) {
        $this->client = new Client([
            'host' => $host,
            'port' => $port,
            'timeout' => $timeout,
            'read_write_timeout' => $timeout
        ]);
    }

    public function getServiceName(): string
    {
        return 'redis';
    }

    public function getHealthReport(): HealthReport
    {
        $builder = new HealthReportBuilder();
        $builder->setServiceName($this->getServiceName());
        try {
            /** @var Status $response */
            $response = $this->client->ping();
            if ($response->getPayload() === 'PONG') {
                $builder->setHealthy();
            }
        } catch (Exception $e) {
            $builder->setUnhealthy();
            $this->logger->error($e->getMessage(), ['exception' => $e]);
        }

        return $builder->buildHealthReport();
    }
}
