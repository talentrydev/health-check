<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\HealthChecker;

use Talentry\HealthCheck\HealthReport\HealthReport;

interface HealthChecker
{
    public function getServiceName(): string;
    public function getHealthReport(): HealthReport;
}
