<?php

declare(strict_types=1);

namespace Talentry\HealthCheck;

use Talentry\HealthCheck\Factory\HealthCheckerFactory;
use Talentry\HealthCheck\HealthReport\HealthReport;

class HealthCheck
{
    public function __construct(
        private readonly HealthCheckerFactory $factory,
    ) {
    }

    public function performHealthCheck(array $services = [], ?string $reportName = null): HealthReport
    {
        return $this->factory->generate($services, $reportName)->getHealthReport();
    }

    public function isServiceSupported(string $service): bool
    {
        return $this->factory->isServiceSupported($service);
    }
}
