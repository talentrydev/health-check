<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\SymfonyBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Talentry\HealthCheck\Factory\HealthCheckerFactory;

class RegisterHealthCheckersCompilerPass implements CompilerPassInterface
{
    public const string TAG_HEALTH_CHECKER = 'healthCheck.healthChecker';

    public function process(ContainerBuilder $container): void
    {
        $factory = $container->findDefinition(HealthCheckerFactory::class);

        foreach ($container->findTaggedServiceIds(self::TAG_HEALTH_CHECKER) as $id => $service) {
            $factory->addMethodCall('addHealthChecker', [new Reference($id)]);
        }
    }
}
