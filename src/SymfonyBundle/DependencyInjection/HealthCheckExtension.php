<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\SymfonyBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Talentry\HealthCheck\Controller\HealthCheckController;
use Talentry\HealthCheck\HealthChecker\HealthChecker;
use Talentry\HealthCheck\HealthChecker\MysqlHealthChecker;
use Talentry\HealthCheck\HealthChecker\RedisHealthChecker;
use Talentry\HealthCheck\SymfonyBundle\DependencyInjection\Compiler\RegisterHealthCheckersCompilerPass;

class HealthCheckExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $config = $this->processConfiguration(new Configuration(), $configs);
        $controller = $container->getDefinition(HealthCheckController::class);
        $controller->setArgument('$serviceName', $config['service_name']);

        if (!$config['mysql']['enabled']) {
            $container->removeDefinition(MysqlHealthChecker::class);
        } else {
            $mysql = $container->getDefinition(MysqlHealthChecker::class);
            $mysql->setArgument(0, $config['mysql']['host']);
            $mysql->setArgument(1, $config['mysql']['port']);
            $mysql->setArgument(2, $config['mysql']['user']);
            $mysql->setArgument(3, $config['mysql']['password']);
            $mysql->setArgument(4, $config['mysql']['database']);
        }

        if (!$config['redis']['enabled']) {
            $container->removeDefinition(RedisHealthChecker::class);
        } else {
            $redis = $container->getDefinition(RedisHealthChecker::class);
            $redis->setArgument(0, $config['redis']['host']);
            $redis->setArgument(1, $config['redis']['port']);
        }

        $container
            ->registerForAutoconfiguration(HealthChecker::class)
            ->addTag(RegisterHealthCheckersCompilerPass::TAG_HEALTH_CHECKER);
    }
}
