<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\SymfonyBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Talentry\HealthCheck\SymfonyBundle\DependencyInjection\Compiler\RegisterHealthCheckersCompilerPass;

class HealthCheckBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new RegisterHealthCheckersCompilerPass());
    }
}
