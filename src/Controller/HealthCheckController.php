<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Talentry\HealthCheck\HealthCheck;

class HealthCheckController extends AbstractController
{
    public function __construct(
        private readonly HealthCheck $healthCheck,
        private readonly string $serviceName = 'API',
    ) {
    }

    public function healthAction(Request $request): Response
    {
        $response = ['status' => '👌'];
        $status = Response::HTTP_OK;

        $services = $this->getServicesFromRequest($request);
        if ($services || $request->query->get('dependencies')) {
            //if dependencies=1 is passed, we perform health check on all services
            //(when passing an empty array to the health check service, it will run a health check on all dependencies)
            $report = $this->healthCheck->performHealthCheck($services, $this->serviceName);
            $response = $report->toArray();
            if (!$report->isHealthy()) {
                $status = Response::HTTP_INTERNAL_SERVER_ERROR;
            }
        }

        return new JsonResponse($response, $status);
    }

    private function getServicesFromRequest(Request $request): array
    {
        $services = [];
        foreach ($request->query->all() as $service => $enabled) {
            if ($enabled && $this->healthCheck->isServiceSupported($service)) {
                $services[] = $service;
            }
        }

        return $services;
    }
}
