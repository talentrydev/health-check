<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\HealthReport;

class CompositeHealthReport implements HealthReport
{
    /**
     * @var HealthReport[]
     */
    private array $components;

    public function __construct(
        private readonly string $name,
    ) {
    }

    public function addComponent(HealthReport $component): void
    {
        $this->components[] = $component;
    }

    public function getServiceName(): string
    {
        return $this->name;
    }

    public function isHealthy(): bool
    {
        foreach ($this->components as $component) {
            if (!$component->isHealthy()) {
                return false;
            }
        }

        return true;
    }

    public function getDependencies(): array
    {
        return $this->components;
    }

    public function toArray(): array
    {
        $array = [
            'service' => $this->getServiceName(),
            'healthy' => $this->isHealthy(),
            'dependencies' => [],
        ];

        foreach ($this->getDependencies() as $dependency) {
            $array['dependencies'][] = $dependency->toArray();
        }

        return $array;
    }

    public function getDetails(): array
    {
        return [];
    }
}
