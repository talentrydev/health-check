<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\HealthReport;

use Exception;

class HealthReportBuilderException extends Exception
{
}
