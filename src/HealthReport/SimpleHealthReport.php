<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\HealthReport;

class SimpleHealthReport implements HealthReport
{
    public function __construct(
        private readonly string $serviceName,
        private readonly bool $healthy,
        private readonly array $details = [],
    ) {
    }

    public function getServiceName(): string
    {
        return $this->serviceName;
    }

    public function isHealthy(): bool
    {
        return $this->healthy;
    }

    public function getDetails(): array
    {
        return $this->details;
    }

    public function toArray(): array
    {
        return [
            'service' => $this->getServiceName(),
            'healthy' => $this->isHealthy(),
            'details' => $this->getDetails(),
        ];
    }
}
