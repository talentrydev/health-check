<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\HealthReport;

interface HealthReport
{
    public function getServiceName(): string;
    public function isHealthy(): bool;
    public function getDetails(): array;
    public function toArray(): array;
}
