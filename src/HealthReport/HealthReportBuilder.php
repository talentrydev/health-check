<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\HealthReport;

class HealthReportBuilder
{
    private string $serviceName;
    private bool $healthy = false;
    private array $details = [];

    public function setHealthy(): void
    {
        $this->healthy = true;
    }

    public function setUnhealthy(): void
    {
        $this->healthy = false;
    }

    public function setServiceName(string $serviceName): void
    {
        $this->serviceName = $serviceName;
    }

    public function setDetails(array $details): void
    {
        $this->details = $details;
    }

    public function buildHealthReport(): HealthReport
    {
        if (!isset($this->serviceName)) {
            throw new HealthReportBuilderException('You must call setServiceName before requesting HealthReport');
        }

        return new SimpleHealthReport($this->serviceName, $this->healthy, $this->details);
    }
}
