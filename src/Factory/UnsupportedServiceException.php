<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\Factory;

use Exception;

class UnsupportedServiceException extends Exception
{
}
