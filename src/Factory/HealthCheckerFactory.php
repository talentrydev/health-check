<?php

declare(strict_types=1);

namespace Talentry\HealthCheck\Factory;

use Talentry\HealthCheck\HealthChecker\CompositeHealthChecker;
use Talentry\HealthCheck\HealthChecker\HealthChecker;

class HealthCheckerFactory
{
    /**
     * @var HealthChecker[]
     */
    private array $healthCheckers = [];

    public function generate(array $services = [], ?string $reportName = null): HealthChecker
    {
        $numberOfServices = count($services);
        if ($numberOfServices === 0) {
            $healthCheckers = $this->healthCheckers;
        } else {
            $healthCheckers = $this->getHealthCheckersByServiceNames($services);
        }

        $compositeHealthChecker = new CompositeHealthChecker($reportName ?: join(',', $services));
        foreach ($healthCheckers as $healthChecker) {
            $compositeHealthChecker->addComponent($healthChecker);
        }

        return $compositeHealthChecker;
    }

    private function getHealthCheckersByServiceNames(array $serviceNames): array
    {
        $healthCheckers = [];
        foreach ($serviceNames as $serviceName) {
            $healthCheckers[] = $this->getHealthCheckerByServiceName($serviceName);
        }

        return $healthCheckers;
    }

    private function getHealthCheckerByServiceName(string $serviceName): HealthChecker
    {
        foreach ($this->healthCheckers as $healthChecker) {
            if ($healthChecker->getServiceName() === $serviceName) {
                return $healthChecker;
            }
        }

        throw new UnsupportedServiceException($serviceName);
    }

    public function isServiceSupported(string $service): bool
    {
        foreach ($this->healthCheckers as $healthChecker) {
            if ($healthChecker->getServiceName() === $service) {
                return true;
            }
        }

        return false;
    }

    public function addHealthChecker(HealthChecker $healthChecker): void
    {
        $this->healthCheckers[] = $healthChecker;
    }
}
